# Guia para Criar um Repositório no GitLab e Configurar uma Pipeline

Este guia fornece instruções passo a passo sobre como criar um repositório no GitLab e configurar uma pipeline simples que executa o comando "echo Hello World".

## Passos

### Passo 1: Criar uma Conta no GitLab

Se você ainda não possui uma conta no GitLab, crie uma em [GitLab.com](https://gitlab.com/users/sign_in).

### Passo 2: Configurar o GitLab Runner com Docker

1. Instale o Docker no seu sistema se ainda não estiver instalado.

1. Suba o container "gitlab/gitlab-runner".

```bash
docker run -d --name runner-docker --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /path/to/runner/config:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest
```

2. No GitLab, vá para o seu projeto e clique em "Configurações" no menu lateral.

3. Selecione "CI/CD" e expanda a seção "Runners".

4. Copie o token exibido na seção "Setup a specific Runner manually".

5. Execute o seguinte comando no terminal, substituindo <TOKEN> pelo token copiado:

```bash
docker exec -it runner-docker \
  gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token TOKEN \
  --clone-url https://gitlab.com/ \
  --executor docker \
  --docker-image "docker:latest" \
  --docker-privileged
```

### Passo 3: Criar um Novo Projeto

1. Faça login na sua conta do GitLab.
2. No menu superior, clique em **"+"** e selecione **"Novo projeto"**.
3. Escolha a opção **"Importar projeto"** ou selecione **"Vazio"** para iniciar um novo projeto.

### Passo 4: Configurar o Repositório Local

Abra o terminal e navegue até o diretório do seu projeto ou crie um novo diretório.

```bash
git init
git add .
git commit -m "Commit inicial"
```

### Passo 5: Conectar o Repositório Local ao GitLab

Copie o URL do repositório GitLab e execute os comandos no terminal:

```bash
git remote add origin https://gitlab.com/seu-usuario/seu-projeto.git
git push -u origin master
```

### Passo 6: Criar um Arquivo .gitlab-ci.yml

No diretório do seu projeto, crie um arquivo chamado .gitlab-ci.yml para configurar a pipeline.

```bash
# .gitlab-ci.yml

stages:
  - build

hello_world:
  stage: build
  script:
    - echo "Hello World"

```

### Passo 7: Commit e Push do Arquivo .gitlab-ci.yml

Adicione e envie o arquivo .gitlab-ci.yml ao seu repositório:

```bash
git add .gitlab-ci.yml
git commit -m "Adicionar arquivo .gitlab-ci.yml"
git push
```

### Passo 8: Verificar a Pipeline

1. No GitLab, vá até o seu projeto.
2. No menu lateral, clique em "CI/CD".
3. Aguarde até que a execução da pipeline seja concluída.

Se tudo estiver configurado corretamente, você verá a mensagem "Hello World" no log da pipeline.

### Conclusão

Parabéns! Você criou um repositório no GitLab e configurou uma pipeline simples que executa o comando "echo Hello World".

**Observação:** Certifique-se de adaptar as informações do usuário, projeto e outras configurações conforme necessário para o seu caso específico.
